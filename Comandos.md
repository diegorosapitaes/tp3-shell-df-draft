# Práctica 3:


Para comenzar se abre el “git bash” y con el comando “cd Desktop” nos dirigimos al escritorio; allí se crea una carpeta con un nombre a preferencia, como por ejemplo Práctica3, ingresamos a la carpeta creadza con el comando “cd Practica3” y una vez allí realizamos un "git clone (dirección de repositorio suministrado por la cátedra)”, el comando "git clone" lo que hace es clonar en el directorio donde nos encontremos, todo el contenido del repositorio el cual especificamos con la dirección de URL ingresada en el comando. Con estos pasos ya nos encontramos en el escritorio, dentro del directorio que creamos y con los archivos necesarios para comenzar la práctica.
Para realizar algunas acciones de la práctica también es necesario trabajar con la consola de windows “CMD” para realizar el cambio de nombre de los archivos y copiar los archivos de un directorio a otro.
A continuación se especificará la lista de comando utilizada para realizar el inciso 1.c de la práctica 3:


+ Consola CMD:


ROBOCOPY C:\Users\diego\Desktop\Practica3
C:\Users\diego\Desktop tp3-data (Copia la carpeta tp3-data de "origen" a "destino")


+ Consola Linux:

mv tp3-data tp3.data1

mv tp3.data1 Desktop/Practica3


Ahora tenemos una copia en Practica3 de la carpeta tp3-data que será necesario más adelante.


Luego es necesario descargar un paquete denominado RenameRegex “RR.exe”, para poder modificar la lista de archivos especificados, el paquete se descarga de la dirección https://github.com/nicjansma/rename-regex/downloads


Una vez descargado, abrimos la consola de windows, nos dirigimos hacia el directorio donde se encuentran las imágenes que próximamente cambiaremos el nombre Desktop/Practica3/tp3-data/fake/ y arrastramos el paquete RR.exe a la consola CMD para poder utilizarlo.


+ Consola CMD:

RR.exe * “mid” “img” (Cambia la parte que dice “real” del nombre de todos los archivos en la carpeta por “img”)

ROBOCOPY Desktop/Practica3/tp3-data1/fake/ Desktop/Practica3/tp3-data/fake/ mid* (Copia todos los archivos desde “origen” ha “destino” que empiezan con “mid”)


+ Consola Linux:

cd Desktop/Practica3/tp3-data/fake
ls -l> renameFake.dat (Crea un archivo de salida con todos los archivos dentro de la carpeta)

cd ..

cd real

+ Consola CMD:

cd ..

cd real

Arrastramos el archivo RR.exe a la consola CMD para poder utilizarlo

RR.exe * “real” “img”

ROBOCOPY Desktop/Practica3/tp3-data1/real/ Desktop/Practica3/tp3-data/real/ mid* (Copia todos los archivos desde “origen” ha “destino” que empiezan con “mid”)

+ Consola Linux:

ls -l> renameReal.dat (Crea un archivo de salida con todos los archivos dentro de la carpeta)

cd ..

cd ..

mkdir dataSet (Crea una carpeta llamada dataSet)

mv tp3-data/fake/img* dataSet

mv tp3-data/fake/ren* dataSet

mv tp3-data/real/img* dataSet

mv tp3-data/real/ren* dataSet

tar -cf dataSet.tar dataSet (Crea una compresión .tar de
la carpeta dataSet)

mv dataSet 14-06-2020dataSet (Cambia de Nombre)

Finalmente se han realizado los pasos requeridos en el inciso de la práctica.
